
import { initializeApp } from "firebase/app";
import { getFirestore} from "firebase/firestore";
import { getAuth } from "firebase/auth";
import 'firebase/compat/auth'; //v9
import firebase from 'firebase/compat/app';
import { getFunctions } from 'firebase/functions';
import { getStorage } from "firebase/storage";



const firebaseConfig = {
  apiKey: "AIzaSyDZpK8wDfo00fNzKAZKDIWRC0ViWW3Mowg",
  authDomain: "wanderline-c2eb5.firebaseapp.com",
  databaseURL: "https://wanderline-c2eb5-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "wanderline-c2eb5",
  storageBucket: "wanderline-c2eb5.appspot.com",
  messagingSenderId: "703857732895",
  appId: "1:703857732895:web:cf811d952219eeead8d42a",
  measurementId: "G-ZB649M918B",
};


if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);

  
}

// console.log("loggin", user, user.uid);

const app = initializeApp(firebaseConfig);

const storage = getStorage(app);

const db = getFirestore(app);


// app.settings({
//   experimentalForceLongPolling: true,
// })

const Auth = getAuth();

const functions = getFunctions(app);


export { db, storage, Auth, firebase, functions };


 



