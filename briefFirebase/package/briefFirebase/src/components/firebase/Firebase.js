
import { initializeApp } from "firebase/app";
import { getFirestore} from "firebase/firestore";
import { getAuth } from "firebase/auth";
import 'firebase/compat/auth'; //v9
import firebase from 'firebase/compat/app';
import { getFunctions } from 'firebase/functions';
import { getStorage } from "firebase/storage";



const firebaseConfig = {
  apiKey: "",
  authDomain: "",
  databaseURL: "",
  projectId: "",
  storageBucket: "",
  messagingSenderId: "",
  appId: "",
  measurementId: "",
};


if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);

  
}

// console.log("loggin", user, user.uid);

const app = initializeApp(firebaseConfig);

const storage = getStorage(app);

const db = getFirestore(app);


// app.settings({
//   experimentalForceLongPolling: true,
// })

const Auth = getAuth();

const functions = getFunctions(app);


export { db, storage, Auth, firebase, functions };


 



